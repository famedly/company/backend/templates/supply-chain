# Supply chain
This repository contains an aggregation of all of Famedly's `cargo vet` audits across our repositories

## Adding your repository as a source
Sources for audits are stored in the `sources.list` file, which is a list of HTTPS URLs pointing to audit files, separated by newlines. To add your repository to the list, add a line containing a URL to repositories audit file to `sources.list`. For public projects it's relatively straigtforward, and requires adding a URL like `https://gitlab.com/famedly/company/backend/services/frobnicator/-/raw/supply-chain/audits.toml`. If it's a private project, you'll need to add a URL to the GitLab API endpoint for getting the audit file, like so: `https://gitlab.com/api/v4/projects/12345678/repository/files/supply-chain%2Faudits.toml/raw?ref=main`, where "12345678" should be replaced with your project's numeric ID.

## Adding this audit list to your repository
To include this list of audits in your repository add the following to the `supply-chain/config.toml` file:
```toml
[imports.famedly]
url = "https://gitlab.com/famedly/company/backend/templates/supply-chain/-/raw/main/audits.toml"
```

## Triggering aggregation manually
If your need your audit to be aggregated before the next automated CI run, you can trigger the scheduled pipeline manually. See the [GitLab docs] for instructions.

[GitLab docs]: https://docs.gitlab.com/ee/ci/pipelines/schedules.html#run-manually
